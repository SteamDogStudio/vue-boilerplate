import './styles/app.scss';

import { Vue } from 'vue-property-decorator';

import { router } from './app/app.router';
import App from './app/App.vue';

export class AppModule {
	constructor() {
		this.bootstrap();
	}

	private async bootstrap(): Promise<Vue> {
		return new Vue({
			el: '#app',
			render: create => create(App),
			router: router(),
		});
	}
}

const app = new AppModule();
