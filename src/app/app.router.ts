import { Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';

import { Homepage } from './pages';

Vue.use(VueRouter);

export function router() {
	return new VueRouter({
		mode: 'history',
		routes: [{
			component: Homepage,
			path: '',
		}],
	});
}
